CREATE OR REPLACE FUNCTION fn_fixsequance()
  RETURNS text AS
$BODY$
DECLARE
mytables RECORD;
query_result text;
query_max INTEGER;
 
BEGIN
query_result='';
FOR mytables IN 
SELECT  S.relname AS seq_name,C.attname,T.relname AS tablename
FROM pg_class AS S, pg_depend AS D, pg_class AS T, pg_attribute AS C
WHERE S.relkind = 'S'
    AND S.oid = D.objid
    AND D.refobjid = T.oid
    AND D.refobjid = C.attrelid
    AND D.refobjsubid = C.attnum
  LOOP
 
	query_result ='select COALESCE(max('||quote_ident(mytables.attname)||'),1) as seq_max from '||quote_ident(mytables.tablename)||';';
	EXECUTE query_result INTO query_max;
	query_result = 'ALTER SEQUENCE '||quote_ident(mytables.seq_name)||' RESTART WITH '||query_max;
	EXECUTE query_result;	
END LOOP;
 
  RETURN 'ok';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
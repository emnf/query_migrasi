 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_official".spt_id || '012'::text AS t_id_transaksi,
    "00_00_00_reff_setor_official".setorpajret_via_bayar AS t_id_via_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '01'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 255
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '01'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 255
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '01'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 256
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '02'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 258
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '02'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 257
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '03'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 260
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '03'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 261
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '03'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 262
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '03'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 259
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '04'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 264
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '04'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 266
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '04'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 263
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '04'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 265
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '04'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '04'::text THEN 267
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 271
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 274
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 273
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 268
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '04'::text THEN 274
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '05'::text THEN 269
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '06'::text THEN 275
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '05'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '07'::text THEN 270
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 281
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 276
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 282
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 283
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '04'::text THEN 284
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '05'::text THEN 285
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '06'::text THEN 286
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '07'::text THEN 280
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '08'::text THEN 277
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '09'::text THEN 278
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '06'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '10'::text THEN 279
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '07'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 287
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '08'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 288
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 294
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 295
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 289
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 292
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '04'::text THEN 291
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '05'::text THEN 293
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '09'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '06'::text THEN 290
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '10'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 296
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '11'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 297
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '12'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 298
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '13'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 299
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '14'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 300
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '14'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 301
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '14'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 302
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '15'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 306
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '15'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 304
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '15'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 305
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '16'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 307
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '16'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 309
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '16'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '02'::text THEN 310
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '16'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '03'::text THEN 311
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '16'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '04'::text THEN 308
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '17'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 312
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '18'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 313
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '19'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 315
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '19'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '01'::text THEN 314
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '20'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 317
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '20'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 316
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '21'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 318
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '22'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 319
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '23'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 320
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '24'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 321
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '25'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 322
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '26'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 323
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '27'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 324
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '28'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 325
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '29'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 326
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '30'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 327
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '31'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 328
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '32'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 329
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '33'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 330
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '34'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 331
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '35'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 332
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '36'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 333
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '37'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 334
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '38'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 335
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '39'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 336
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '40'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 337
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '41'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 338
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '42'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 339
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '43'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 340
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '44'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 341
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '45'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 342
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '46'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 343
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '47'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 344
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '48'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 345
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '49'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 346
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '50'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 347
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '51'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 348
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '52'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 349
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '53'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 350
            WHEN "00_00_00_reff_setor_official".korek_rincian::text = '54'::text AND "00_00_00_reff_setor_official".korek_sub1::text = '00'::text THEN 351
            ELSE 351
        END AS t_id_rekening_pembayaran,
    1 AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_jlh_bayar AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_official"
  WHERE "00_00_00_reff_setor_official".spt_jenis_pajakretribusi = 10
 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_self".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_self".spt_id || '003'::text AS t_id_transaksi,
    "00_00_00_reff_setor_self".setorpajret_via_bayar AS t_id_via_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '320'::text THEN 439
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '32'::text THEN 52
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '39'::text THEN 416
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '321'::text THEN 48
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '37'::text THEN 417
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '22'::text THEN 233
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '26'::text THEN 45
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '29'::text THEN 415
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '35'::text THEN 422
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '33'::text THEN 421
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '23'::text THEN 45
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '24'::text THEN 45
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '34'::text THEN 46
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '11'::text THEN 46
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '251'::text THEN 441
            ELSE NULL::integer
        END AS t_id_rekening_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '251'::text THEN 3
            ELSE 1
        END AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_self".setorpajret_jlh_bayar AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_self"
  WHERE "00_00_00_reff_setor_self".spt_jenis_pajakretribusi = 3
 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_self".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_self".spt_id || '007'::text AS t_id_transaksi,
    "00_00_00_reff_setor_self".setorpajret_via_bayar AS t_id_via_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '102'::text THEN 431
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '325'::text THEN 432
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '324'::text THEN 431
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '104'::text THEN 432
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '109'::text THEN 424
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '247'::text THEN 185
            ELSE NULL::integer
        END AS t_id_rekening_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '247'::text THEN 3
            ELSE 1
        END AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_self".setorpajret_jlh_bayar AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_self"
  WHERE "00_00_00_reff_setor_self".spt_jenis_pajakretribusi = 7
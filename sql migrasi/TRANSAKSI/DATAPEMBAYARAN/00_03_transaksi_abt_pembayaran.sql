 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_official".spt_id || '008'::text AS t_id_transaksi,
    "00_00_00_reff_setor_official".setorpajret_via_bayar AS t_id_via_pembayaran,
    102 AS t_id_rekening_pembayaran,
    2 AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_jlh_bayar AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_official"
  WHERE "00_00_00_reff_setor_official".spt_jenis_pajakretribusi = 8
  ORDER BY "00_00_00_reff_setor_official".setorpajret_tgl_bayar
 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_self".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_self".spt_id || '001'::text AS t_id_transaksi,
    "00_00_00_reff_setor_self".setorpajret_via_bayar AS t_id_via_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '0'::text THEN 38
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '11'::text THEN 38
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '10'::text THEN 403
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '6'::text THEN 398
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '7'::text THEN 402
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '12'::text THEN 402
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '5'::text THEN 401
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '4'::text THEN 399
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '13'::text THEN 38
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '253'::text THEN 130
            ELSE NULL::integer
        END AS t_id_rekening_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_self".sprsd_kode_rek::text = '253'::text THEN 3
            ELSE 1
        END AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_self".setorpajret_jlh_bayar AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_self"
  WHERE "00_00_00_reff_setor_self".spt_jenis_pajakretribusi = 1
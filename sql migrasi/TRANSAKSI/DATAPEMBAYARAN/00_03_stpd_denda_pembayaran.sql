 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_official".setorpajret_id_penetapan || '666'::text AS t_id_transaksi,
    "00_00_00_reff_setor_official".setorpajret_via_bayar AS t_id_via_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_official".netapajrek_kode_rek::text = '252'::text THEN 139
            WHEN "00_00_00_reff_setor_official".netapajrek_kode_rek::text = '253'::text THEN 130
            WHEN "00_00_00_reff_setor_official".netapajrek_kode_rek::text = '251'::text THEN 441
            WHEN "00_00_00_reff_setor_official".netapajrek_kode_rek::text = '247'::text THEN 185
            ELSE NULL::integer
        END AS t_id_rekening_pembayaran,
    1 AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_official".spt_dt_pajak::integer * "00_00_00_reff_setor_official".netapajrek_persen_tarif::integer / 100 AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_official"
  WHERE "00_00_00_reff_setor_official".setorpajret_jenis_ketetapan = 3
 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_official".spt_id || '004'::text AS t_id_transaksi,
    "00_00_00_reff_setor_official".setorpajret_via_bayar AS t_id_via_pembayaran,
        CASE
            WHEN "00_00_00_reff_setor_official".korek_id::text = '65'::text THEN 353
            WHEN "00_00_00_reff_setor_official".korek_id::text = '43'::text THEN 353
            WHEN "00_00_00_reff_setor_official".korek_id::text = '51'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '47'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '64'::text THEN 356
            WHEN "00_00_00_reff_setor_official".korek_id::text = '55'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '54'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '45'::text THEN 359
            WHEN "00_00_00_reff_setor_official".korek_id::text = '308'::text THEN 352
            WHEN "00_00_00_reff_setor_official".korek_id::text = '60'::text THEN 367
            WHEN "00_00_00_reff_setor_official".korek_id::text = '50'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '53'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '48'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '313'::text THEN 360
            WHEN "00_00_00_reff_setor_official".korek_id::text = '44'::text THEN 357
            WHEN "00_00_00_reff_setor_official".korek_id::text = '317'::text THEN 370
            WHEN "00_00_00_reff_setor_official".korek_id::text = '318'::text THEN 365
            WHEN "00_00_00_reff_setor_official".korek_id::text = '52'::text THEN 358
            WHEN "00_00_00_reff_setor_official".korek_id::text = '56'::text THEN 363
            ELSE NULL::integer
        END AS t_id_rekening_pembayaran,
    1 AS t_id_jenis_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_jlh_bayar AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_official"
  WHERE "00_00_00_reff_setor_official".spt_jenis_pajakretribusi = 4
 SELECT ''::text AS id,
    ''::text AS uuid,
    ''::text AS t_no_pembayaran,
    "00_00_00_reff_setor_official".setorpajret_tgl_bayar AS t_tgl_pembayaran,
    NULL::text AS t_tgl_bayar_bunga,
    "00_00_00_reff_setor_official".spt_id || '006'::text AS t_id_transaksi,
    "00_00_00_reff_setor_official".setorpajret_via_bayar AS t_id_via_pembayaran,
    433 AS t_id_rekening_pembayaran,
    1 AS t_id_jenis_pembayaran,
    ( SELECT sum(b.spt_dt_pajak) AS sum
           FROM "00_00_00_reff_setor_official" b
          WHERE "00_00_00_reff_setor_official".spt_id = b.spt_id) AS t_jumlah_pembayaran,
    NULL::text AS t_jumlah_bulan_bunga,
    NULL::text AS t_tgl_pembayaran_bunga,
    NULL::text AS t_id_bank,
    NULL::text AS created_by,
    NULL::text AS created_at,
    NULL::text AS updated_at,
    NULL::text AS deleted_at_by
   FROM "00_00_00_reff_setor_official"
  WHERE "00_00_00_reff_setor_official".spt_jenis_pajakretribusi = 6
  GROUP BY "00_00_00_reff_setor_official".spt_id, "00_00_00_reff_setor_official".setorpajret_tgl_bayar, "00_00_00_reff_setor_official".setorpajret_via_bayar
  ORDER BY "00_00_00_reff_setor_official".setorpajret_tgl_bayar DESC
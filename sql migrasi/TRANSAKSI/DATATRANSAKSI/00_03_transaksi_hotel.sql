 SELECT (a.spt_id || '001'::text) ||
        CASE
            WHEN a.korek_rincian::text = '02'::text THEN '399'::text
            WHEN a.korek_rincian::text = '04'::text THEN '401'::text
            WHEN a.korek_rincian::text = '05'::text THEN '398'::text
            WHEN a.korek_rincian::text = '07'::text THEN '402'::text
            WHEN a.korek_rincian::text = '08'::text THEN '402'::text
            WHEN a.korek_rincian::text = '09'::text THEN '402'::text
            WHEN a.korek_rincian::text = '12'::text THEN '403'::text
            WHEN a.korek_rincian::text = '14'::text THEN '38'::text
            WHEN a.korek_rincian::text = '13'::text THEN '404'::text
            WHEN a.korek_rincian::text = '06'::text THEN '400'::text
            WHEN a.korek_rincian::text = '00'::text THEN '228'::text
            ELSE '228'::text
        END AS id,
    a.spt_id || '001'::text AS id2,
    ''::text AS uuid,
    (a.spt_idwpwr || '001'::text) ||
        CASE
            WHEN a.korek_rincian::text = '02'::text THEN '399'::text
            WHEN a.korek_rincian::text = '04'::text THEN '401'::text
            WHEN a.korek_rincian::text = '05'::text THEN '398'::text
            WHEN a.korek_rincian::text = '07'::text THEN '402'::text
            WHEN a.korek_rincian::text = '08'::text THEN '402'::text
            WHEN a.korek_rincian::text = '09'::text THEN '402'::text
            WHEN a.korek_rincian::text = '12'::text THEN '403'::text
            WHEN a.korek_rincian::text = '14'::text THEN '38'::text
            WHEN a.korek_rincian::text = '13'::text THEN '404'::text
            WHEN a.korek_rincian::text = '06'::text THEN '400'::text
            WHEN a.korek_rincian::text = '00'::text THEN '228'::text
            ELSE '228'::text
        END AS t_id_objek,
    1 AS t_id_jenis_objek,
        CASE
            WHEN a.korek_rincian::text = '02'::text THEN 399
            WHEN a.korek_rincian::text = '04'::text THEN 401
            WHEN a.korek_rincian::text = '05'::text THEN 398
            WHEN a.korek_rincian::text = '07'::text THEN 402
            WHEN a.korek_rincian::text = '08'::text THEN 402
            WHEN a.korek_rincian::text = '09'::text THEN 402
            WHEN a.korek_rincian::text = '12'::text THEN 403
            WHEN a.korek_rincian::text = '14'::text THEN 38
            WHEN a.korek_rincian::text = '13'::text THEN 404
            WHEN a.korek_rincian::text = '06'::text THEN 400
            WHEN a.korek_rincian::text = '00'::text THEN 228
            ELSE 228
        END AS t_id_rekening,
    1 AS t_id_jenis_surat,
    a.spt_nomor AS t_no_pendataan,
    a.spt_tgl_proses AS t_tgl_pendataan,
    a.spt_periode AS t_tahun_pajak,
    a.spt_periode_jual1 AS t_masa_awal,
    a.spt_periode_jual2 AS t_masa_akhir,
    a.spt_dt_jumlah AS t_dasar_pengenaan,
    a.spt_dt_persen_tarif AS t_tarif_persen,
    a.spt_dt_tarif_dasar AS t_tarif_dasar,
    ''::text AS t_volume,
    ''::text AS t_satuan,
    a.spt_tgl_jatuhtempo AS t_tgl_jatuh_tempo,
    a.spt_dt_pajak AS t_jumlah_pajak,
    ''::text AS t_persen_kenaikan,
    ''::text AS t_jumlah_kenaikan,
    a.spt_dt_denda AS t_jumlah_bunga,
    '1'::text AS created_by_pendataan,
    NULL::bigint AS t_no_penetapan,
    NULL::date AS t_tgl_penetapan,
    ''::text AS created_by_penetapan,
    ''::text AS t_keterangan,
    ''::text AS t_kode_bayar,
    ''::text AS t_id_lhp,
    ''::text AS t_id_lhp_detail,
    ''::text AS is_esptpd,
    '1'::text AS channel_id,
    ''::text AS created_at,
    ''::text AS updated_at,
    ''::text AS deleted_at,
    ''::text AS t_id_satker,
    ''::text AS t_jenis_sumur,
    ''::text AS t_nomor_sumur,
    ''::text AS t_zona_air,
    ''::text AS t_kelompok_air,
    ''::text AS t_kompensasi
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
  WHERE a.spt_jenis_pajakretribusi = 1
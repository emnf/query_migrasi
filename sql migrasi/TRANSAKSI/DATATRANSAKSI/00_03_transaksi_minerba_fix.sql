 SELECT a.spt_id || '006433'::text AS id,
    a.spt_id || '006'::text AS id2,
    ''::text AS uuid,
    a.spt_idwpwr || '006433'::text AS t_id_objek,
    6 AS t_id_jenis_objek,
    433 AS t_id_rekening,
        CASE
            WHEN a.ketspt_kode::text = 'O'::text THEN 1
            ELSE 1
        END AS t_id_jenis_surat,
    a.spt_nomor AS t_no_pendataan,
    a.spt_tgl_proses AS t_tgl_pendataan,
    a.spt_periode AS t_tahun_pajak,
    a.spt_periode_jual1 AS t_masa_awal,
    a.spt_periode_jual2 AS t_masa_akhir,
    NULL::double precision AS t_dasar_pengenaan,
    a.spt_dt_persen_tarif AS t_tarif_persen,
    NULL::double precision AS t_tarif_dasar,
    ''::text AS t_volume,
    ''::text AS t_satuan,
    a.spt_tgl_jatuhtempo AS t_tgl_jatuh_tempo,
    ( SELECT sum(b_1.spt_dt_pajak) AS sum
           FROM v_spt_one_one b_1
          WHERE a.spt_id = b_1.spt_id) AS t_jumlah_pajak,
    ''::text AS t_persen_kenaikan,
    ''::text AS t_jumlah_kenaikan,
    a.spt_dt_denda AS t_jumlah_bunga,
    '1'::text AS created_by_pendataan,
    NULL::bigint AS t_no_penetapan,
    NULL::date AS t_tgl_penetapan,
    ''::text AS created_by_penetapan,
    ''::text AS t_keterangan,
    ''::text AS t_kode_bayar,
    ''::text AS t_id_lhp,
    ''::text AS t_id_lhp_detail,
    ''::text AS is_esptpd,
    '1'::text AS channel_id,
    ''::text AS created_at,
    ''::text AS updated_at,
    ''::text AS deleted_at,
    ''::text AS t_id_satker,
    ''::text AS t_jenis_sumur,
    ''::text AS t_nomor_sumur,
    ''::text AS t_zona_air,
    ''::text AS t_kelompok_air,
    ''::text AS t_kompensasi
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
  WHERE a.spt_jenis_pajakretribusi = 6
  GROUP BY a.spt_id, a.spt_idwpwr, a.ketspt_kode, a.spt_nomor, a.spt_tgl_proses, a.spt_periode, a.spt_periode_jual1, a.spt_periode_jual2, a.spt_dt_jumlah, a.spt_dt_persen_tarif, a.spt_dt_tarif_dasar, a.spt_tgl_jatuhtempo, a.spt_dt_denda, a.spt_jenis_pemungutan
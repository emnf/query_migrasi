 SELECT (a.spt_id || '004'::text) ||
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN '353'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '01'::text THEN '352'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN '353'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '03'::text THEN '354'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '04'::text THEN '355'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN '356'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '01'::text THEN '357'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '02'::text THEN '357'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '03'::text THEN '357'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '04'::text THEN '359'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '05'::text THEN '359'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '06'::text THEN '86'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN '87'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '01'::text THEN '86'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '02'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '03'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '04'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '05'::text THEN '358'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '06'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '07'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '08'::text THEN '357'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN '235'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '01'::text THEN '358'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '02'::text THEN '360'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '03'::text THEN '361'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN '88'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN '367'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '01'::text THEN '89'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '01'::text THEN '91'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '02'::text THEN '92'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '03'::text THEN '93'::text
            WHEN a.korek_rincian::text = '08'::text AND a.korek_sub1::text = '00'::text THEN '31'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN '87'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '01'::text THEN '362'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '02'::text THEN '363'::text
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN '371'::text
            ELSE '353'::text
        END AS id,
    a.spt_id || '004'::text AS id2,
    ''::text AS uuid,
    (a.spt_idwpwr || '004'::text) ||
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN '353'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '01'::text THEN '352'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN '353'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '03'::text THEN '354'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '04'::text THEN '355'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN '356'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '01'::text THEN '357'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '02'::text THEN '357'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '03'::text THEN '357'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '04'::text THEN '359'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '05'::text THEN '359'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '06'::text THEN '86'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN '87'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '01'::text THEN '86'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '02'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '03'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '04'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '05'::text THEN '358'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '06'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '07'::text THEN '357'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '08'::text THEN '357'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN '235'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '01'::text THEN '358'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '02'::text THEN '360'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '03'::text THEN '361'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN '88'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN '367'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '01'::text THEN '89'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '01'::text THEN '91'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '02'::text THEN '92'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '03'::text THEN '93'::text
            WHEN a.korek_rincian::text = '08'::text AND a.korek_sub1::text = '00'::text THEN '31'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN '87'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '01'::text THEN '362'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '02'::text THEN '363'::text
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN '371'::text
            ELSE '353'::text
        END AS t_id_objek,
    4 AS t_id_jenis_objek,
        CASE
            WHEN a.spt_dt_korek::text = '65'::text THEN 353
            WHEN a.spt_dt_korek::text = '43'::text THEN 353
            WHEN a.spt_dt_korek::text = '51'::text THEN 357
            WHEN a.spt_dt_korek::text = '47'::text THEN 357
            WHEN a.spt_dt_korek::text = '64'::text THEN 356
            WHEN a.spt_dt_korek::text = '55'::text THEN 357
            WHEN a.spt_dt_korek::text = '54'::text THEN 357
            WHEN a.spt_dt_korek::text = '45'::text THEN 359
            WHEN a.spt_dt_korek::text = '308'::text THEN 352
            WHEN a.spt_dt_korek::text = '60'::text THEN 367
            WHEN a.spt_dt_korek::text = '50'::text THEN 357
            WHEN a.spt_dt_korek::text = '53'::text THEN 357
            WHEN a.spt_dt_korek::text = '48'::text THEN 357
            WHEN a.spt_dt_korek::text = '313'::text THEN 360
            WHEN a.spt_dt_korek::text = '44'::text THEN 357
            WHEN a.spt_dt_korek::text = '317'::text THEN 370
            WHEN a.spt_dt_korek::text = '318'::text THEN 365
            WHEN a.spt_dt_korek::text = '52'::text THEN 358
            WHEN a.spt_dt_korek::text = '56'::text THEN 363
            ELSE NULL::integer
        END AS t_id_rekening,
        CASE
            WHEN a.ketspt_kode::text = 'O'::text THEN 1
            ELSE 2
        END AS t_id_jenis_surat,
    a.spt_nomor AS t_no_pendataan,
    a.spt_tgl_proses AS t_tgl_pendataan,
    a.spt_periode AS t_tahun_pajak,
    a.spt_periode_jual1 AS t_masa_awal,
    a.spt_periode_jual2 AS t_masa_akhir,
    a.spt_dt_jumlah AS t_dasar_pengenaan,
    a.spt_dt_persen_tarif AS t_tarif_persen,
    a.spt_dt_tarif_dasar AS t_tarif_dasar,
    ''::text AS t_volume,
    ''::text AS t_satuan,
    a.spt_tgl_jatuhtempo AS t_tgl_jatuh_tempo,
    a.spt_dt_pajak AS t_jumlah_pajak,
    ''::text AS t_persen_kenaikan,
    ''::text AS t_jumlah_kenaikan,
    a.spt_dt_denda AS t_jumlah_bunga,
    '1'::text AS created_by_pendataan,
    a.spt_nomor AS t_no_penetapan,
    b.netapajrek_tgl AS t_tgl_penetapan,
    ''::text AS created_by_penetapan,
    ''::text AS t_keterangan,
    ''::text AS t_kode_bayar,
    ''::text AS t_id_lhp,
    ''::text AS t_id_lhp_detail,
    ''::text AS is_esptpd,
    '1'::text AS channel_id,
    ''::text AS created_at,
    ''::text AS updated_at,
    ''::text AS deleted_at,
    ''::text AS t_id_satker,
    ''::text AS t_jenis_sumur,
    ''::text AS t_nomor_sumur,
    ''::text AS t_zona_air,
    ''::text AS t_kelompok_air,
    ''::text AS t_kompensasi
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
  WHERE a.spt_jenis_pajakretribusi = 4
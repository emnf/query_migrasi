 SELECT penetapan_pajak_retribusi.netapajrek_id::text || '666'::text AS id,
    ''::text AS uuid,
        CASE
            WHEN spt.spt_jenis_pajakretribusi::text = '1'::text THEN spt.spt_idwpwr || '001'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '2'::text THEN spt.spt_idwpwr || '002'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '3'::text THEN spt.spt_idwpwr || '003'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '4'::text THEN spt.spt_idwpwr || '004'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '5'::text THEN spt.spt_idwpwr || '005'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '6'::text THEN spt.spt_idwpwr || '006'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '7'::text THEN spt.spt_idwpwr || '007'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '8'::text THEN spt.spt_idwpwr || '008'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '9'::text THEN spt.spt_idwpwr || '009'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '10'::text THEN spt.spt_idwpwr || '012'::text
            ELSE NULL::text
        END AS t_id_objek,
        CASE
            WHEN spt.spt_jenis_pajakretribusi::text = '1'::text THEN 1
            WHEN spt.spt_jenis_pajakretribusi::text = '2'::text THEN 2
            WHEN spt.spt_jenis_pajakretribusi::text = '3'::text THEN 3
            WHEN spt.spt_jenis_pajakretribusi::text = '4'::text THEN 4
            WHEN spt.spt_jenis_pajakretribusi::text = '5'::text THEN 5
            WHEN spt.spt_jenis_pajakretribusi::text = '6'::text THEN 6
            WHEN spt.spt_jenis_pajakretribusi::text = '7'::text THEN 7
            WHEN spt.spt_jenis_pajakretribusi::text = '8'::text THEN 8
            WHEN spt.spt_jenis_pajakretribusi::text = '9'::text THEN 9
            WHEN spt.spt_jenis_pajakretribusi::text = '10'::text THEN 12
            ELSE NULL::integer
        END AS t_id_jenis_objek,
        CASE
            WHEN spt.spt_jenis_pajakretribusi::text = '1'::text THEN 130
            WHEN spt.spt_jenis_pajakretribusi::text = '2'::text THEN 139
            WHEN spt.spt_jenis_pajakretribusi::text = '3'::text THEN 441
            WHEN spt.spt_jenis_pajakretribusi::text = '7'::text THEN 185
            ELSE NULL::integer
        END AS t_id_rekening,
    penetapan_pajak_retribusi.netapajrek_jenis_ketetapan AS t_id_jenis_surat,
    row_number() OVER (PARTITION BY spt.spt_periode ORDER BY spt.spt_id, spt.spt_periode) AS t_no_pendataan,
    penetapan_pajak_retribusi.netapajrek_tgl AS t_tgl_pendataan,
    spt.spt_periode AS t_tahun_pajak,
    penetapan_pajak_retribusi.netapajrek_tgl AS t_masa_awal,
    penetapan_pajak_retribusi.netapajrek_tgl_jatuh_tempo AS t_masa_akhir,
    spt_detail.spt_dt_pajak AS t_dasar_pengenaan,
    penetapan_pajak_retribusi.netapajrek_persen_tarif::real AS t_tarif_persen,
    '0'::double precision AS t_tarif_dasar,
    ''::text AS t_volume,
    ''::text AS t_satuan,
    penetapan_pajak_retribusi.netapajrek_tgl_jatuh_tempo AS t_tgl_jatuh_tempo,
    penetapan_pajak_retribusi.netapajrek_besaran AS t_jumlah_pajak,
    ''::text AS t_persen_kenaikan,
    ''::text AS t_jumlah_kenaikan,
    '0'::double precision AS t_jumlah_bunga,
    '1'::text AS created_by_pendataan,
    row_number() OVER (PARTITION BY spt.spt_periode ORDER BY spt.spt_id) AS t_no_penetapan,
    penetapan_pajak_retribusi.netapajrek_tgl AS t_tgl_penetapan,
    '1'::text AS created_by_penetapan,
    ''::text AS t_keterangan,
		
    '8171'::text||CASE
            WHEN spt.spt_jenis_pajakretribusi::text = '1'::text THEN '01'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '2'::text THEN '02'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '3'::text THEN '03'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '4'::text THEN '04'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '5'::text THEN '05'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '6'::text THEN '06'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '7'::text THEN '07'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '8'::text THEN '08'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '9'::text THEN '09'::text
            WHEN spt.spt_jenis_pajakretribusi::text = '10'::text THEN '12'::text
            ELSE NULL::text
        END || '03'::text || RIGHT(spt.spt_periode::text,2)::text || lpad(row_number() OVER (PARTITION BY spt.spt_periode ORDER BY spt.spt_id)::text, 5,'0')::text AS t_kode_bayar,
		
		
    ''::text AS t_id_lhp,
    ''::text AS t_id_lhp_detail,
    ''::text AS is_esptpd,
    ''::text AS channel_id,
    ''::text AS created_at,
    ''::text AS updated_at,
    ''::text AS deleted_at,
    ''::text AS t_id_satker,
    ''::text AS t_jenis_sumur,
    ''::text AS t_nomor_sumur,
    ''::text AS t_zona_air,
    ''::text AS t_kelompok_air,
    ''::text AS t_kompensasi
   FROM penetapan_pajak_retribusi
     LEFT JOIN spt ON penetapan_pajak_retribusi.netapajrek_id_spt = spt.spt_id
     LEFT JOIN spt_detail ON spt.spt_id = spt_detail.spt_dt_id_spt
     LEFT JOIN setoran_pajak_retribusi ON penetapan_pajak_retribusi.netapajrek_id = setoran_pajak_retribusi.setorpajret_id_penetapan
  WHERE penetapan_pajak_retribusi.netapajrek_jenis_ketetapan = 3 AND penetapan_pajak_retribusi.netapajrek_is_denda = 1 AND setoran_pajak_retribusi.setorpajret_jenis_ketetapan = 3
 SELECT (a.spt_id || '012'::text) ||
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN '255'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '01'::text THEN '255'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN '256'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN '258'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '03'::text THEN '257'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN '260'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '01'::text THEN '261'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '02'::text THEN '262'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '03'::text THEN '259'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN '264'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '01'::text THEN '266'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '02'::text THEN '263'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '03'::text THEN '265'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '04'::text THEN '267'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN '271'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '01'::text THEN '274'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '02'::text THEN '273'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '03'::text THEN '268'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '04'::text THEN '274'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '05'::text THEN '269'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '06'::text THEN '275'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '07'::text THEN '270'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN '281'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '01'::text THEN '276'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '02'::text THEN '282'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '03'::text THEN '283'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '04'::text THEN '284'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '05'::text THEN '285'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '06'::text THEN '286'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '07'::text THEN '280'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '08'::text THEN '277'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '09'::text THEN '278'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '10'::text THEN '279'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN '287'::text
            WHEN a.korek_rincian::text = '08'::text AND a.korek_sub1::text = '00'::text THEN '288'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN '294'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '01'::text THEN '295'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '02'::text THEN '289'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '03'::text THEN '292'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '04'::text THEN '291'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '05'::text THEN '293'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '06'::text THEN '290'::text
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN '296'::text
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN '297'::text
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN '298'::text
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '01'::text THEN '299'::text
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '00'::text THEN '300'::text
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '01'::text THEN '301'::text
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '02'::text THEN '302'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN '306'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '01'::text THEN '304'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '02'::text THEN '305'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN '307'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '01'::text THEN '309'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '02'::text THEN '310'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '03'::text THEN '311'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '04'::text THEN '308'::text
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN '312'::text
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN '313'::text
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN '315'::text
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '01'::text THEN '314'::text
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN '317'::text
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN '316'::text
            WHEN a.korek_rincian::text = '21'::text AND a.korek_sub1::text = '00'::text THEN '318'::text
            WHEN a.korek_rincian::text = '22'::text AND a.korek_sub1::text = '00'::text THEN '319'::text
            WHEN a.korek_rincian::text = '23'::text AND a.korek_sub1::text = '00'::text THEN '320'::text
            WHEN a.korek_rincian::text = '24'::text AND a.korek_sub1::text = '00'::text THEN '321'::text
            WHEN a.korek_rincian::text = '25'::text AND a.korek_sub1::text = '00'::text THEN '322'::text
            WHEN a.korek_rincian::text = '26'::text AND a.korek_sub1::text = '00'::text THEN '323'::text
            WHEN a.korek_rincian::text = '27'::text AND a.korek_sub1::text = '00'::text THEN '324'::text
            WHEN a.korek_rincian::text = '28'::text AND a.korek_sub1::text = '00'::text THEN '325'::text
            WHEN a.korek_rincian::text = '29'::text AND a.korek_sub1::text = '00'::text THEN '326'::text
            WHEN a.korek_rincian::text = '30'::text AND a.korek_sub1::text = '00'::text THEN '327'::text
            WHEN a.korek_rincian::text = '31'::text AND a.korek_sub1::text = '00'::text THEN '328'::text
            WHEN a.korek_rincian::text = '32'::text AND a.korek_sub1::text = '00'::text THEN '329'::text
            WHEN a.korek_rincian::text = '33'::text AND a.korek_sub1::text = '00'::text THEN '330'::text
            WHEN a.korek_rincian::text = '34'::text AND a.korek_sub1::text = '00'::text THEN '331'::text
            WHEN a.korek_rincian::text = '35'::text AND a.korek_sub1::text = '00'::text THEN '332'::text
            WHEN a.korek_rincian::text = '36'::text AND a.korek_sub1::text = '00'::text THEN '333'::text
            WHEN a.korek_rincian::text = '37'::text AND a.korek_sub1::text = '00'::text THEN '334'::text
            WHEN a.korek_rincian::text = '38'::text AND a.korek_sub1::text = '00'::text THEN '335'::text
            WHEN a.korek_rincian::text = '39'::text AND a.korek_sub1::text = '00'::text THEN '336'::text
            WHEN a.korek_rincian::text = '40'::text AND a.korek_sub1::text = '00'::text THEN '337'::text
            WHEN a.korek_rincian::text = '41'::text AND a.korek_sub1::text = '00'::text THEN '338'::text
            WHEN a.korek_rincian::text = '42'::text AND a.korek_sub1::text = '00'::text THEN '339'::text
            WHEN a.korek_rincian::text = '43'::text AND a.korek_sub1::text = '00'::text THEN '340'::text
            WHEN a.korek_rincian::text = '44'::text AND a.korek_sub1::text = '00'::text THEN '341'::text
            WHEN a.korek_rincian::text = '45'::text AND a.korek_sub1::text = '00'::text THEN '342'::text
            WHEN a.korek_rincian::text = '46'::text AND a.korek_sub1::text = '00'::text THEN '343'::text
            WHEN a.korek_rincian::text = '47'::text AND a.korek_sub1::text = '00'::text THEN '344'::text
            WHEN a.korek_rincian::text = '48'::text AND a.korek_sub1::text = '00'::text THEN '345'::text
            WHEN a.korek_rincian::text = '49'::text AND a.korek_sub1::text = '00'::text THEN '346'::text
            WHEN a.korek_rincian::text = '50'::text AND a.korek_sub1::text = '00'::text THEN '347'::text
            WHEN a.korek_rincian::text = '51'::text AND a.korek_sub1::text = '00'::text THEN '348'::text
            WHEN a.korek_rincian::text = '52'::text AND a.korek_sub1::text = '00'::text THEN '349'::text
            WHEN a.korek_rincian::text = '53'::text AND a.korek_sub1::text = '00'::text THEN '350'::text
            WHEN a.korek_rincian::text = '54'::text AND a.korek_sub1::text = '00'::text THEN '351'::text
            ELSE '351'::text
        END AS id,
    a.spt_id || '012'::text AS id2,
    ''::text AS uuid,
    (a.spt_idwpwr || '012'::text) ||
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN '255'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '01'::text THEN '255'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN '256'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN '258'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '03'::text THEN '257'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN '260'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '01'::text THEN '261'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '02'::text THEN '262'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '03'::text THEN '259'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN '264'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '01'::text THEN '266'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '02'::text THEN '263'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '03'::text THEN '265'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '04'::text THEN '267'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN '271'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '01'::text THEN '274'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '02'::text THEN '273'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '03'::text THEN '268'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '04'::text THEN '274'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '05'::text THEN '269'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '06'::text THEN '275'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '07'::text THEN '270'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN '281'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '01'::text THEN '276'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '02'::text THEN '282'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '03'::text THEN '283'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '04'::text THEN '284'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '05'::text THEN '285'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '06'::text THEN '286'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '07'::text THEN '280'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '08'::text THEN '277'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '09'::text THEN '278'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '10'::text THEN '279'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN '287'::text
            WHEN a.korek_rincian::text = '08'::text AND a.korek_sub1::text = '00'::text THEN '288'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN '294'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '01'::text THEN '295'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '02'::text THEN '289'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '03'::text THEN '292'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '04'::text THEN '291'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '05'::text THEN '293'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '06'::text THEN '290'::text
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN '296'::text
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN '297'::text
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN '298'::text
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '01'::text THEN '299'::text
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '00'::text THEN '300'::text
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '01'::text THEN '301'::text
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '02'::text THEN '302'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN '306'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '01'::text THEN '304'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '02'::text THEN '305'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN '307'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '01'::text THEN '309'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '02'::text THEN '310'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '03'::text THEN '311'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '04'::text THEN '308'::text
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN '312'::text
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN '313'::text
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN '315'::text
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '01'::text THEN '314'::text
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN '317'::text
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN '316'::text
            WHEN a.korek_rincian::text = '21'::text AND a.korek_sub1::text = '00'::text THEN '318'::text
            WHEN a.korek_rincian::text = '22'::text AND a.korek_sub1::text = '00'::text THEN '319'::text
            WHEN a.korek_rincian::text = '23'::text AND a.korek_sub1::text = '00'::text THEN '320'::text
            WHEN a.korek_rincian::text = '24'::text AND a.korek_sub1::text = '00'::text THEN '321'::text
            WHEN a.korek_rincian::text = '25'::text AND a.korek_sub1::text = '00'::text THEN '322'::text
            WHEN a.korek_rincian::text = '26'::text AND a.korek_sub1::text = '00'::text THEN '323'::text
            WHEN a.korek_rincian::text = '27'::text AND a.korek_sub1::text = '00'::text THEN '324'::text
            WHEN a.korek_rincian::text = '28'::text AND a.korek_sub1::text = '00'::text THEN '325'::text
            WHEN a.korek_rincian::text = '29'::text AND a.korek_sub1::text = '00'::text THEN '326'::text
            WHEN a.korek_rincian::text = '30'::text AND a.korek_sub1::text = '00'::text THEN '327'::text
            WHEN a.korek_rincian::text = '31'::text AND a.korek_sub1::text = '00'::text THEN '328'::text
            WHEN a.korek_rincian::text = '32'::text AND a.korek_sub1::text = '00'::text THEN '329'::text
            WHEN a.korek_rincian::text = '33'::text AND a.korek_sub1::text = '00'::text THEN '330'::text
            WHEN a.korek_rincian::text = '34'::text AND a.korek_sub1::text = '00'::text THEN '331'::text
            WHEN a.korek_rincian::text = '35'::text AND a.korek_sub1::text = '00'::text THEN '332'::text
            WHEN a.korek_rincian::text = '36'::text AND a.korek_sub1::text = '00'::text THEN '333'::text
            WHEN a.korek_rincian::text = '37'::text AND a.korek_sub1::text = '00'::text THEN '334'::text
            WHEN a.korek_rincian::text = '38'::text AND a.korek_sub1::text = '00'::text THEN '335'::text
            WHEN a.korek_rincian::text = '39'::text AND a.korek_sub1::text = '00'::text THEN '336'::text
            WHEN a.korek_rincian::text = '40'::text AND a.korek_sub1::text = '00'::text THEN '337'::text
            WHEN a.korek_rincian::text = '41'::text AND a.korek_sub1::text = '00'::text THEN '338'::text
            WHEN a.korek_rincian::text = '42'::text AND a.korek_sub1::text = '00'::text THEN '339'::text
            WHEN a.korek_rincian::text = '43'::text AND a.korek_sub1::text = '00'::text THEN '340'::text
            WHEN a.korek_rincian::text = '44'::text AND a.korek_sub1::text = '00'::text THEN '341'::text
            WHEN a.korek_rincian::text = '45'::text AND a.korek_sub1::text = '00'::text THEN '342'::text
            WHEN a.korek_rincian::text = '46'::text AND a.korek_sub1::text = '00'::text THEN '343'::text
            WHEN a.korek_rincian::text = '47'::text AND a.korek_sub1::text = '00'::text THEN '344'::text
            WHEN a.korek_rincian::text = '48'::text AND a.korek_sub1::text = '00'::text THEN '345'::text
            WHEN a.korek_rincian::text = '49'::text AND a.korek_sub1::text = '00'::text THEN '346'::text
            WHEN a.korek_rincian::text = '50'::text AND a.korek_sub1::text = '00'::text THEN '347'::text
            WHEN a.korek_rincian::text = '51'::text AND a.korek_sub1::text = '00'::text THEN '348'::text
            WHEN a.korek_rincian::text = '52'::text AND a.korek_sub1::text = '00'::text THEN '349'::text
            WHEN a.korek_rincian::text = '53'::text AND a.korek_sub1::text = '00'::text THEN '350'::text
            WHEN a.korek_rincian::text = '54'::text AND a.korek_sub1::text = '00'::text THEN '351'::text
            ELSE '351'::text
        END AS t_id_objek,
    12 AS t_id_jenis_objek,
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN 255
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '01'::text THEN 255
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN 256
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN 258
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '03'::text THEN 257
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN 260
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '01'::text THEN 261
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '02'::text THEN 262
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '03'::text THEN 259
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN 264
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '01'::text THEN 266
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '02'::text THEN 263
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '03'::text THEN 265
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '04'::text THEN 267
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN 271
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '01'::text THEN 274
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '02'::text THEN 273
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '03'::text THEN 268
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '04'::text THEN 274
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '05'::text THEN 269
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '06'::text THEN 275
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '07'::text THEN 270
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN 281
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '01'::text THEN 276
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '02'::text THEN 282
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '03'::text THEN 283
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '04'::text THEN 284
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '05'::text THEN 285
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '06'::text THEN 286
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '07'::text THEN 280
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '08'::text THEN 277
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '09'::text THEN 278
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '10'::text THEN 279
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN 287
            WHEN a.korek_rincian::text = '08'::text AND a.korek_sub1::text = '00'::text THEN 288
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN 294
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '01'::text THEN 295
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '02'::text THEN 289
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '03'::text THEN 292
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '04'::text THEN 291
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '05'::text THEN 293
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '06'::text THEN 290
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN 296
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN 297
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN 298
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '01'::text THEN 299
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '00'::text THEN 300
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '01'::text THEN 301
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '02'::text THEN 302
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN 306
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '01'::text THEN 304
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '02'::text THEN 305
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN 307
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '01'::text THEN 309
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '02'::text THEN 310
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '03'::text THEN 311
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '04'::text THEN 308
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN 312
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN 313
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN 315
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '01'::text THEN 314
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN 317
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN 316
            WHEN a.korek_rincian::text = '21'::text AND a.korek_sub1::text = '00'::text THEN 318
            WHEN a.korek_rincian::text = '22'::text AND a.korek_sub1::text = '00'::text THEN 319
            WHEN a.korek_rincian::text = '23'::text AND a.korek_sub1::text = '00'::text THEN 320
            WHEN a.korek_rincian::text = '24'::text AND a.korek_sub1::text = '00'::text THEN 321
            WHEN a.korek_rincian::text = '25'::text AND a.korek_sub1::text = '00'::text THEN 322
            WHEN a.korek_rincian::text = '26'::text AND a.korek_sub1::text = '00'::text THEN 323
            WHEN a.korek_rincian::text = '27'::text AND a.korek_sub1::text = '00'::text THEN 324
            WHEN a.korek_rincian::text = '28'::text AND a.korek_sub1::text = '00'::text THEN 325
            WHEN a.korek_rincian::text = '29'::text AND a.korek_sub1::text = '00'::text THEN 326
            WHEN a.korek_rincian::text = '30'::text AND a.korek_sub1::text = '00'::text THEN 327
            WHEN a.korek_rincian::text = '31'::text AND a.korek_sub1::text = '00'::text THEN 328
            WHEN a.korek_rincian::text = '32'::text AND a.korek_sub1::text = '00'::text THEN 329
            WHEN a.korek_rincian::text = '33'::text AND a.korek_sub1::text = '00'::text THEN 330
            WHEN a.korek_rincian::text = '34'::text AND a.korek_sub1::text = '00'::text THEN 331
            WHEN a.korek_rincian::text = '35'::text AND a.korek_sub1::text = '00'::text THEN 332
            WHEN a.korek_rincian::text = '36'::text AND a.korek_sub1::text = '00'::text THEN 333
            WHEN a.korek_rincian::text = '37'::text AND a.korek_sub1::text = '00'::text THEN 334
            WHEN a.korek_rincian::text = '38'::text AND a.korek_sub1::text = '00'::text THEN 335
            WHEN a.korek_rincian::text = '39'::text AND a.korek_sub1::text = '00'::text THEN 336
            WHEN a.korek_rincian::text = '40'::text AND a.korek_sub1::text = '00'::text THEN 337
            WHEN a.korek_rincian::text = '41'::text AND a.korek_sub1::text = '00'::text THEN 338
            WHEN a.korek_rincian::text = '42'::text AND a.korek_sub1::text = '00'::text THEN 339
            WHEN a.korek_rincian::text = '43'::text AND a.korek_sub1::text = '00'::text THEN 340
            WHEN a.korek_rincian::text = '44'::text AND a.korek_sub1::text = '00'::text THEN 341
            WHEN a.korek_rincian::text = '45'::text AND a.korek_sub1::text = '00'::text THEN 342
            WHEN a.korek_rincian::text = '46'::text AND a.korek_sub1::text = '00'::text THEN 343
            WHEN a.korek_rincian::text = '47'::text AND a.korek_sub1::text = '00'::text THEN 344
            WHEN a.korek_rincian::text = '48'::text AND a.korek_sub1::text = '00'::text THEN 345
            WHEN a.korek_rincian::text = '49'::text AND a.korek_sub1::text = '00'::text THEN 346
            WHEN a.korek_rincian::text = '50'::text AND a.korek_sub1::text = '00'::text THEN 347
            WHEN a.korek_rincian::text = '51'::text AND a.korek_sub1::text = '00'::text THEN 348
            WHEN a.korek_rincian::text = '52'::text AND a.korek_sub1::text = '00'::text THEN 349
            WHEN a.korek_rincian::text = '53'::text AND a.korek_sub1::text = '00'::text THEN 350
            WHEN a.korek_rincian::text = '54'::text AND a.korek_sub1::text = '00'::text THEN 351
            ELSE 351
        END AS t_id_rekening,
        9 AS t_id_jenis_surat,
    a.spt_nomor AS t_no_pendataan,
    a.spt_tgl_proses AS t_tgl_pendataan,
    a.spt_periode AS t_tahun_pajak,
    a.spt_periode_jual1 AS t_masa_awal,
    a.spt_periode_jual2 AS t_masa_akhir,
    a.spt_dt_jumlah AS t_dasar_pengenaan,
    a.spt_dt_persen_tarif AS t_tarif_persen,
    a.spt_dt_tarif_dasar AS t_tarif_dasar,
    ''::text AS t_volume,
    ''::text AS t_satuan,
    a.spt_tgl_jatuhtempo AS t_tgl_jatuh_tempo,
    a.spt_dt_pajak AS t_jumlah_pajak,
    ''::text AS t_persen_kenaikan,
    ''::text AS t_jumlah_kenaikan,
    a.spt_dt_denda AS t_jumlah_bunga,
    '1'::text AS created_by_pendataan,
    a.spt_nomor AS t_no_penetapan,
    b.netapajrek_tgl AS t_tgl_penetapan,
    ''::text AS created_by_penetapan,
    ''::text AS t_keterangan,
    ''::text AS t_kode_bayar,
    ''::text AS t_id_lhp,
    ''::text AS t_id_lhp_detail,
    ''::text AS is_esptpd,
    '1'::text AS channel_id,
    ''::text AS created_at,
    ''::text AS updated_at,
    ''::text AS deleted_at,
    ''::text AS t_id_satker,
    ''::text AS t_jenis_sumur,
    ''::text AS t_nomor_sumur,
    ''::text AS t_zona_air,
    ''::text AS t_kelompok_air,
    ''::text AS t_kompensasi
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
  WHERE a.spt_jenis_pajakretribusi = 10
 SELECT ''::text AS id,
    ''::text AS uuid,
    "00_00_00_reff_setor_official".spt_id || '006'::text AS t_id_transaksi,
    "00_00_00_reff_setor_official".korek_id,
        CASE
            WHEN "00_00_00_reff_setor_official".korek_id::text = '77'::text THEN 392
            WHEN "00_00_00_reff_setor_official".korek_id::text = '80'::text THEN 388
            WHEN "00_00_00_reff_setor_official".korek_id::text = '906'::text THEN 393
            WHEN "00_00_00_reff_setor_official".korek_id::text = '100'::text THEN 375
            WHEN "00_00_00_reff_setor_official".korek_id::text = '99'::text THEN 376
            WHEN "00_00_00_reff_setor_official".korek_id::text = '84'::text THEN 384
            WHEN "00_00_00_reff_setor_official".korek_id::text = '78'::text THEN 390
            WHEN "00_00_00_reff_setor_official".korek_id::text = '88'::text THEN 373
            WHEN "00_00_00_reff_setor_official".korek_id::text = '76'::text THEN 392
            WHEN "00_00_00_reff_setor_official".korek_id::text = '90'::text THEN 170
            WHEN "00_00_00_reff_setor_official".korek_id::text = '83'::text THEN 385
            WHEN "00_00_00_reff_setor_official".korek_id::text = '79'::text THEN 389
            WHEN "00_00_00_reff_setor_official".korek_id::text = '85'::text THEN 383
            WHEN "00_00_00_reff_setor_official".korek_id::text = '89'::text THEN 372
            WHEN "00_00_00_reff_setor_official".korek_id::text = '96'::text THEN 379
            WHEN "00_00_00_reff_setor_official".korek_id::text = '86'::text THEN 382
            WHEN "00_00_00_reff_setor_official".korek_id::text = '75'::text THEN 394
            WHEN "00_00_00_reff_setor_official".korek_id::text = '93'::text THEN 374
            WHEN "00_00_00_reff_setor_official".korek_id::text = '97'::text THEN 378
            WHEN "00_00_00_reff_setor_official".korek_id::text = '74'::text THEN 395
            ELSE NULL::integer
        END AS t_id_rekening,
    "00_00_00_reff_setor_official".spt_dt_volume AS t_volume,
    "00_00_00_reff_setor_official".spt_dt_tarif_dasar AS t_hargapasaran,
    "00_00_00_reff_setor_official".spt_dt_persen_tarif AS t_tarif_persen,
    "00_00_00_reff_setor_official".spt_dt_pajak AS t_jmlh_pajak,
    "00_00_00_reff_setor_official".spt_dt_pajak AS t_jmlh_pembayaran,
    NULL::text AS t_jmlh_bunga,
    NULL::text AS created_at,
    NULL::text AS updated_at
   FROM "00_00_00_reff_setor_official"
  WHERE "00_00_00_reff_setor_official".spt_jenis_pajakretribusi = 6
  ORDER BY "00_00_00_reff_setor_official".setorpajret_tgl_bayar DESC, "00_00_00_reff_setor_official".spt_id DESC
 SELECT (a.spt_id || '003'::text) ||
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '04'::text THEN '45'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN '45'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '03'::text THEN '45'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN '415'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN '416'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN '417'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN '438'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN '422'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN '45'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN '44'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN '50'::text
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN '437'::text
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN '439'::text
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN '48'::text
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '00'::text THEN '47'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN '52'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN '233'::text
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN '51'::text
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN '421'::text
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN '46'::text
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN '422'::text
            ELSE '437'::text
        END AS id,
    a.spt_id || '003'::text AS id2,
    ''::text AS uuid,
    (a.spt_idwpwr || '003'::text) ||
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '04'::text THEN '45'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN '45'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '03'::text THEN '45'::text
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN '415'::text
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN '416'::text
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN '417'::text
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN '438'::text
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN '422'::text
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN '45'::text
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN '44'::text
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN '50'::text
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN '437'::text
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN '439'::text
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN '48'::text
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '00'::text THEN '47'::text
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN '52'::text
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN '233'::text
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN '51'::text
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN '421'::text
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN '46'::text
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN '422'::text
            ELSE 437::text
        END AS t_id_objek,
    3 AS t_id_jenis_objek,
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '04'::text THEN 45
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN 45
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '03'::text THEN 45
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN 415
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN 416
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN 417
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN 438
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN 422
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN 45
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN 44
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN 50
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN 437
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN 439
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN 48
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '00'::text THEN 47
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN 52
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN 233
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN 51
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN 421
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN 46
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN 422
            ELSE 437
        END AS t_id_rekening,
        CASE
            WHEN a.ketspt_kode::text = 'O'::text THEN 1
            ELSE 1
        END AS t_id_jenis_surat,
    a.spt_nomor AS t_no_pendataan,
    a.spt_tgl_proses AS t_tgl_pendataan,
    a.spt_periode AS t_tahun_pajak,
    a.spt_periode_jual1 AS t_masa_awal,
    a.spt_periode_jual2 AS t_masa_akhir,
    a.spt_dt_jumlah AS t_dasar_pengenaan,
    a.spt_dt_persen_tarif AS t_tarif_persen,
    a.spt_dt_tarif_dasar AS t_tarif_dasar,
    ''::text AS t_volume,
    ''::text AS t_satuan,
    a.spt_tgl_jatuhtempo AS t_tgl_jatuh_tempo,
    a.spt_dt_pajak AS t_jumlah_pajak,
    ''::text AS t_persen_kenaikan,
    ''::text AS t_jumlah_kenaikan,
    a.spt_dt_denda AS t_jumlah_bunga,
    '1'::text AS created_by_pendataan,
    NULL::bigint AS t_no_penetapan,
    NULL::date AS t_tgl_penetapan,
    ''::text AS created_by_penetapan,
    ''::text AS t_keterangan,
    ''::text AS t_kode_bayar,
    ''::text AS t_id_lhp,
    ''::text AS t_id_lhp_detail,
    ''::text AS is_esptpd,
    '1'::text AS channel_id,
    ''::text AS created_at,
    ''::text AS updated_at,
    ''::text AS deleted_at,
    ''::text AS t_id_satker,
    ''::text AS t_jenis_sumur,
    ''::text AS t_nomor_sumur,
    ''::text AS t_zona_air,
    ''::text AS t_kelompok_air,
    ''::text AS t_kompensasi
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
  WHERE a.spt_jenis_pajakretribusi = 3
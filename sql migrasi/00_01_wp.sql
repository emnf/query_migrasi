 SELECT wp_wr.wp_wr_id AS id,
    ''::text AS uuid,
    wp_wr.wp_wr_tgl_terima_form AS t_tgl_daftar,
    upper(wp_wr.wp_wr_jenis::text) AS t_jenispendaftaran,
    wp_wr.wp_wr_gol AS t_golongan_daftar,
    wp_wr.wp_wr_no_urut AS t_no_daftar,
    ''::text AS t_npwp,
    wp_wr.wp_wr_no_urut AS t_nib_nik,
    ''::text AS t_siup,
    wp_wr.wp_wr_nama AS t_nama,
    wp_wr.wp_wr_almt AS t_jalan,
    '000'::text AS t_rt,
    '000'::text AS t_rw,
        CASE
            WHEN wp_wr.wp_wr_kd_camat::text = '1'::text THEN 0
            ELSE 81
        END AS t_id_provinsi,
        CASE
            WHEN wp_wr.wp_wr_kd_camat::text = '1'::text THEN 0
            ELSE 8171
        END AS t_id_kabupaten,
        CASE
            WHEN wp_wr.wp_wr_kd_camat::text = '2'::text THEN 8171020
            WHEN wp_wr.wp_wr_kd_camat::text = '3'::text THEN 8171030
            WHEN wp_wr.wp_wr_kd_camat::text = '4'::text THEN 8171031
            WHEN wp_wr.wp_wr_kd_camat::text = '5'::text THEN 8171010
            WHEN wp_wr.wp_wr_kd_camat::text = '6'::text THEN 8171021
            ELSE 0
        END AS t_id_kecamatan,
        CASE
            WHEN wp_wr.wp_wr_kd_lurah::text = '2'::text THEN 46409
            WHEN wp_wr.wp_wr_kd_lurah::text = '3'::text THEN 46400
            WHEN wp_wr.wp_wr_kd_lurah::text = '4'::text THEN 46399
            WHEN wp_wr.wp_wr_kd_lurah::text = '5'::text THEN 46398
            WHEN wp_wr.wp_wr_kd_lurah::text = '6'::text THEN 46408
            WHEN wp_wr.wp_wr_kd_lurah::text = '7'::text THEN 46401
            WHEN wp_wr.wp_wr_kd_lurah::text = '8'::text THEN 46404
            WHEN wp_wr.wp_wr_kd_lurah::text = '9'::text THEN 46403
            WHEN wp_wr.wp_wr_kd_lurah::text = '10'::text THEN 46407
            WHEN wp_wr.wp_wr_kd_lurah::text = '11'::text THEN 46411
            WHEN wp_wr.wp_wr_kd_lurah::text = '12'::text THEN 46410
            WHEN wp_wr.wp_wr_kd_lurah::text = '13'::text THEN 46406
            WHEN wp_wr.wp_wr_kd_lurah::text = '14'::text THEN 46405
            WHEN wp_wr.wp_wr_kd_lurah::text = '15'::text THEN 46402
            WHEN wp_wr.wp_wr_kd_lurah::text = '16'::text THEN 46426
            WHEN wp_wr.wp_wr_kd_lurah::text = '17'::text THEN 46423
            WHEN wp_wr.wp_wr_kd_lurah::text = '18'::text THEN 46424
            WHEN wp_wr.wp_wr_kd_lurah::text = '19'::text THEN 46425
            WHEN wp_wr.wp_wr_kd_lurah::text = '20'::text THEN 46421
            WHEN wp_wr.wp_wr_kd_lurah::text = '21'::text THEN 46422
            WHEN wp_wr.wp_wr_kd_lurah::text = '22'::text THEN 46420
            WHEN wp_wr.wp_wr_kd_lurah::text = '23'::text THEN 46414
            WHEN wp_wr.wp_wr_kd_lurah::text = '24'::text THEN 46417
            WHEN wp_wr.wp_wr_kd_lurah::text = '25'::text THEN 46412
            WHEN wp_wr.wp_wr_kd_lurah::text = '26'::text THEN 46419
            WHEN wp_wr.wp_wr_kd_lurah::text = '27'::text THEN 46418
            WHEN wp_wr.wp_wr_kd_lurah::text = '28'::text THEN 46416
            WHEN wp_wr.wp_wr_kd_lurah::text = '29'::text THEN 46415
            WHEN wp_wr.wp_wr_kd_lurah::text = '30'::text THEN 46413
            WHEN wp_wr.wp_wr_kd_lurah::text = '31'::text THEN 46397
            WHEN wp_wr.wp_wr_kd_lurah::text = '32'::text THEN 46394
            WHEN wp_wr.wp_wr_kd_lurah::text = '33'::text THEN 46396
            WHEN wp_wr.wp_wr_kd_lurah::text = '34'::text THEN 46395
            WHEN wp_wr.wp_wr_kd_lurah::text = '35'::text THEN 46386
            WHEN wp_wr.wp_wr_kd_lurah::text = '36'::text THEN 46389
            WHEN wp_wr.wp_wr_kd_lurah::text = '37'::text THEN 46389
            WHEN wp_wr.wp_wr_kd_lurah::text = '38'::text THEN 46387
            WHEN wp_wr.wp_wr_kd_lurah::text = '39'::text THEN 46393
            WHEN wp_wr.wp_wr_kd_lurah::text = '40'::text THEN 46388
            WHEN wp_wr.wp_wr_kd_lurah::text = '41'::text THEN 46391
            WHEN wp_wr.wp_wr_kd_lurah::text = '42'::text THEN 46392
            WHEN wp_wr.wp_wr_kd_lurah::text = '43'::text THEN 46390
            WHEN wp_wr.wp_wr_kd_lurah::text = '44'::text THEN 46379
            WHEN wp_wr.wp_wr_kd_lurah::text = '45'::text THEN 46384
            WHEN wp_wr.wp_wr_kd_lurah::text = '46'::text THEN 46382
            WHEN wp_wr.wp_wr_kd_lurah::text = '47'::text THEN 46378
            WHEN wp_wr.wp_wr_kd_lurah::text = '48'::text THEN 46380
            WHEN wp_wr.wp_wr_kd_lurah::text = '49'::text THEN 46383
            WHEN wp_wr.wp_wr_kd_lurah::text = '50'::text THEN 46385
            WHEN wp_wr.wp_wr_kd_lurah::text = '51'::text THEN 46381
            ELSE 0
        END AS t_id_kelurahan,
    wp_wr.wp_wr_kodepos AS t_kode_pos,
    ''::text AS t_email,
    ''::text AS t_no_hp,
    wp_wr.wp_wr_telp AS t_no_telp,
    wp_wr.wp_wr_no_urut AS t_nik_pemilik,
    ''::text AS t_npwp_pemilik,
    wp_wr.wp_wr_nama_milik AS t_nama_pemilik,
    wp_wr.wp_wr_almt_milik AS t_jalan_pemilik,
    '000'::text AS t_rt_pemilik,
    '000'::text AS t_rw_pemilik,
    ''::text AS t_id_provinsi_pemilik,
    ''::text AS t_id_kabupaten_pemilik,
    ''::text AS t_id_kecamatan_pemilik,
    ''::text AS t_id_kelurahan_pemilik,
    ''::text AS t_kode_pos_pemilik,
    ''::text AS t_email_pemilik,
    ''::text AS t_no_hp_pemilik,
    wp_wr.wp_wr_telp_milik AS t_no_telp_pemilik,
    '1'::text AS created_by,
    wp_wr.wp_wr_status_aktif AS t_active,
    wp_wr.wp_wr_tgl_kartu::timestamp without time zone AS created_at,
    wp_wr.wp_wr_tgl_kartu::timestamp without time zone AS updated_at,
    ''::text AS deleted_at,
    row_number() OVER (ORDER BY wp_wr.wp_wr_id) AS idbaru,
    wp_wr.wp_wr_bidang_usaha AS t_bidang_usaha
   FROM wp_wr
  ORDER BY wp_wr.wp_wr_id
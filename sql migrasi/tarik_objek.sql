SELECT
	"00_00_00_00_m_objek".idbaru AS "id", 
	"00_00_00_00_m_objek".uuid, 
	"00_01_wp".idbaru AS t_id_wp, 
	"00_00_00_00_m_objek".t_tgl_daftar_objek, 
	row_number() OVER (ORDER BY "00_00_00_00_m_objek".idbaru) AS t_no_daftar_objek,
	"00_00_00_00_m_objek".t_id_jenis_objek, 
	"00_00_00_00_m_objek".t_id_rekening_objek, 
	"00_00_00_00_m_objek".t_status_objek, 
	"00_00_00_00_m_objek".t_nama_objek, 
	"00_00_00_00_m_objek".t_jalan_objek, 
	"00_00_00_00_m_objek".t_rt_objek, 
	"00_00_00_00_m_objek".t_rw_objek, 
	"00_00_00_00_m_objek".t_id_provinsi_objek, 
	"00_00_00_00_m_objek".t_id_kabupaten_objek, 
	"00_00_00_00_m_objek".t_id_kecamatan_objek, 
	"00_00_00_00_m_objek".t_id_kelurahan_objek, 
	"00_00_00_00_m_objek".t_kode_pos_objek, 
	"00_00_00_00_m_objek".t_latitude_objek, 
	"00_00_00_00_m_objek".t_longitude_objek, 
	"00_00_00_00_m_objek".t_nik_pj, 
	"00_00_00_00_m_objek".t_nama_pj, 
	"00_00_00_00_m_objek".t_alamat_pj, 
	"00_00_00_00_m_objek".t_jabatan_pj, 
	"00_00_00_00_m_objek".t_email_pj, 
	"00_00_00_00_m_objek".t_no_hp_pj, 
	"00_00_00_00_m_objek".created_by, 
	"00_00_00_00_m_objek".t_active, 
	"00_00_00_00_m_objek".t_foto_objek, 
	"00_00_00_00_m_objek".created_at, 
	"00_00_00_00_m_objek".updated_at, 
	"00_00_00_00_m_objek".deleted_at, 
	"00_00_00_00_m_objek".t_bulan
FROM
	"00_00_00_00_m_objek"
	LEFT JOIN
	"00_01_wp"
	ON 
		"00_00_00_00_m_objek".t_id_wp = "00_01_wp"."id"
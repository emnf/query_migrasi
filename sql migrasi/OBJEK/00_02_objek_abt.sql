 SELECT a.spt_idwpwr || '008'::text AS id,
    ''::text AS uuid,
    a.spt_idwpwr AS t_idwp,
    c.wp_wr_tgl_terima_form AS t_tgl_daftar_objek,
    ''::text AS t_no_daftar_objek,
    a.spt_jenis_pajakretribusi AS t_id_jenis_objek,
    102 AS t_id_rekening,
    'Induk'::text AS t_status_objek,
    a.wp_wr_nama AS t_nama_objek,
    a.wp_wr_almt AS t_jalan_tobjek,
    '000'::text AS t_rt_objek,
    '000'::text AS t_rw_objek,
        CASE
            WHEN c.wp_wr_kd_camat::text = '1'::text THEN 0
            ELSE 81
        END AS t_id_provinsi_objek,
        CASE
            WHEN c.wp_wr_kd_camat::text = '1'::text THEN 0
            ELSE 8171
        END AS t_id_kabupaten_objek,
        CASE
            WHEN c.wp_wr_kd_camat::text = '2'::text THEN 8171020
            WHEN c.wp_wr_kd_camat::text = '3'::text THEN 8171030
            WHEN c.wp_wr_kd_camat::text = '4'::text THEN 8171031
            WHEN c.wp_wr_kd_camat::text = '5'::text THEN 8171010
            WHEN c.wp_wr_kd_camat::text = '6'::text THEN 8171021
            ELSE 0
        END AS t_id_kecamatan_objek,
        CASE
            WHEN c.wp_wr_kd_lurah::text = '2'::text THEN 46409
            WHEN c.wp_wr_kd_lurah::text = '3'::text THEN 46400
            WHEN c.wp_wr_kd_lurah::text = '4'::text THEN 46399
            WHEN c.wp_wr_kd_lurah::text = '5'::text THEN 46398
            WHEN c.wp_wr_kd_lurah::text = '6'::text THEN 46408
            WHEN c.wp_wr_kd_lurah::text = '7'::text THEN 46401
            WHEN c.wp_wr_kd_lurah::text = '8'::text THEN 46404
            WHEN c.wp_wr_kd_lurah::text = '9'::text THEN 46403
            WHEN c.wp_wr_kd_lurah::text = '10'::text THEN 46407
            WHEN c.wp_wr_kd_lurah::text = '11'::text THEN 46411
            WHEN c.wp_wr_kd_lurah::text = '12'::text THEN 46410
            WHEN c.wp_wr_kd_lurah::text = '13'::text THEN 46406
            WHEN c.wp_wr_kd_lurah::text = '14'::text THEN 46405
            WHEN c.wp_wr_kd_lurah::text = '15'::text THEN 46402
            WHEN c.wp_wr_kd_lurah::text = '16'::text THEN 46426
            WHEN c.wp_wr_kd_lurah::text = '17'::text THEN 46423
            WHEN c.wp_wr_kd_lurah::text = '18'::text THEN 46424
            WHEN c.wp_wr_kd_lurah::text = '19'::text THEN 46425
            WHEN c.wp_wr_kd_lurah::text = '20'::text THEN 46421
            WHEN c.wp_wr_kd_lurah::text = '21'::text THEN 46422
            WHEN c.wp_wr_kd_lurah::text = '22'::text THEN 46420
            WHEN c.wp_wr_kd_lurah::text = '23'::text THEN 46414
            WHEN c.wp_wr_kd_lurah::text = '24'::text THEN 46417
            WHEN c.wp_wr_kd_lurah::text = '25'::text THEN 46412
            WHEN c.wp_wr_kd_lurah::text = '26'::text THEN 46419
            WHEN c.wp_wr_kd_lurah::text = '27'::text THEN 46418
            WHEN c.wp_wr_kd_lurah::text = '28'::text THEN 46416
            WHEN c.wp_wr_kd_lurah::text = '29'::text THEN 46415
            WHEN c.wp_wr_kd_lurah::text = '30'::text THEN 46413
            WHEN c.wp_wr_kd_lurah::text = '31'::text THEN 46397
            WHEN c.wp_wr_kd_lurah::text = '32'::text THEN 46394
            WHEN c.wp_wr_kd_lurah::text = '33'::text THEN 46396
            WHEN c.wp_wr_kd_lurah::text = '34'::text THEN 46395
            WHEN c.wp_wr_kd_lurah::text = '35'::text THEN 46386
            WHEN c.wp_wr_kd_lurah::text = '36'::text THEN 46389
            WHEN c.wp_wr_kd_lurah::text = '37'::text THEN 46389
            WHEN c.wp_wr_kd_lurah::text = '38'::text THEN 46387
            WHEN c.wp_wr_kd_lurah::text = '39'::text THEN 46393
            WHEN c.wp_wr_kd_lurah::text = '40'::text THEN 46388
            WHEN c.wp_wr_kd_lurah::text = '41'::text THEN 46391
            WHEN c.wp_wr_kd_lurah::text = '42'::text THEN 46392
            WHEN c.wp_wr_kd_lurah::text = '43'::text THEN 46390
            WHEN c.wp_wr_kd_lurah::text = '44'::text THEN 46379
            WHEN c.wp_wr_kd_lurah::text = '45'::text THEN 46384
            WHEN c.wp_wr_kd_lurah::text = '46'::text THEN 46382
            WHEN c.wp_wr_kd_lurah::text = '47'::text THEN 46378
            WHEN c.wp_wr_kd_lurah::text = '48'::text THEN 46380
            WHEN c.wp_wr_kd_lurah::text = '49'::text THEN 46383
            WHEN c.wp_wr_kd_lurah::text = '50'::text THEN 46385
            WHEN c.wp_wr_kd_lurah::text = '51'::text THEN 46381
            ELSE 0
        END AS t_id_kelurahan_objek,
    c.wp_wr_kodepos AS t_kode_pos_objek,
    c.t_latitudeobjek AS t_latitude_objek,
    c.t_longitudeobjek,
    ''::text AS t_nik_pj,
    c.wp_wr_nama_milik AS t_nama_pj,
    c.wp_wr_almt_milik AS t_alamat_pj,
    ''::text AS t_jabatan_pj,
    ''::text AS t_email_pj,
    c.wp_wr_telp_milik AS t_no_hp_pj,
    '1'::text AS created_by,
    c.wp_wr_status_aktif AS t_statusobjek,
    ''::text AS t_foto_objek,
    c.wp_wr_tgl_kartu::timestamp without time zone AS created_at,
    c.wp_wr_tgl_kartu::timestamp without time zone AS updated_at,
    ''::text AS deleted_at,
    'f'::text AS t_bulan
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
     LEFT JOIN wp_wr c ON c.wp_wr_id = a.spt_idwpwr
  WHERE a.spt_jenis_pajakretribusi = 8
  GROUP BY a.spt_idwpwr, c.wp_wr_tgl_terima_form, a.wp_wr_nama, a.wp_wr_almt, c.wp_wr_kd_camat, c.wp_wr_kd_lurah, a.wp_wr_kabupaten, c.wp_wr_telp, a.spt_jenis_pajakretribusi, c.wp_wr_kodepos, c.t_latitudeobjek, c.t_longitudeobjek, c.wp_wr_pejabat, c.wp_wr_status_aktif, c.wp_wr_telp_milik, c.wp_wr_tgl_kartu, a.korek_rincian, a.korek_sub1,c.wp_wr_nama_milik ,c.wp_wr_almt_milik
  ORDER BY c.wp_wr_tgl_terima_form
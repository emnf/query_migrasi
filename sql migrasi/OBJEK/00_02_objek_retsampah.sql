 SELECT a.spt_idwpwr || '012'::text AS id,
    ''::text AS uuid,
    a.spt_idwpwr AS t_idwp,
    c.wp_wr_tgl_terima_form AS t_tgl_daftar_objek,
    ''::text AS t_no_daftar_objek,
    12 AS t_id_jenis_objek,
        CASE
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '00'::text THEN 255
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '01'::text THEN 255
            WHEN a.korek_rincian::text = '01'::text AND a.korek_sub1::text = '02'::text THEN 256
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '00'::text THEN 258
            WHEN a.korek_rincian::text = '02'::text AND a.korek_sub1::text = '03'::text THEN 257
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '00'::text THEN 260
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '01'::text THEN 261
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '02'::text THEN 262
            WHEN a.korek_rincian::text = '03'::text AND a.korek_sub1::text = '03'::text THEN 259
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '00'::text THEN 264
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '01'::text THEN 266
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '02'::text THEN 263
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '03'::text THEN 265
            WHEN a.korek_rincian::text = '04'::text AND a.korek_sub1::text = '04'::text THEN 267
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '00'::text THEN 271
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '01'::text THEN 274
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '02'::text THEN 273
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '03'::text THEN 268
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '04'::text THEN 274
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '05'::text THEN 269
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '06'::text THEN 275
            WHEN a.korek_rincian::text = '05'::text AND a.korek_sub1::text = '07'::text THEN 270
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '00'::text THEN 281
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '01'::text THEN 276
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '02'::text THEN 282
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '03'::text THEN 283
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '04'::text THEN 284
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '05'::text THEN 285
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '06'::text THEN 286
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '07'::text THEN 280
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '08'::text THEN 277
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '09'::text THEN 278
            WHEN a.korek_rincian::text = '06'::text AND a.korek_sub1::text = '10'::text THEN 279
            WHEN a.korek_rincian::text = '07'::text AND a.korek_sub1::text = '00'::text THEN 287
            WHEN a.korek_rincian::text = '08'::text AND a.korek_sub1::text = '00'::text THEN 288
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '00'::text THEN 294
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '01'::text THEN 295
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '02'::text THEN 289
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '03'::text THEN 292
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '04'::text THEN 291
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '05'::text THEN 293
            WHEN a.korek_rincian::text = '09'::text AND a.korek_sub1::text = '06'::text THEN 290
            WHEN a.korek_rincian::text = '10'::text AND a.korek_sub1::text = '00'::text THEN 296
            WHEN a.korek_rincian::text = '11'::text AND a.korek_sub1::text = '00'::text THEN 297
            WHEN a.korek_rincian::text = '12'::text AND a.korek_sub1::text = '00'::text THEN 298
            WHEN a.korek_rincian::text = '13'::text AND a.korek_sub1::text = '01'::text THEN 299
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '00'::text THEN 300
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '01'::text THEN 301
            WHEN a.korek_rincian::text = '14'::text AND a.korek_sub1::text = '02'::text THEN 302
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '00'::text THEN 306
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '01'::text THEN 304
            WHEN a.korek_rincian::text = '15'::text AND a.korek_sub1::text = '02'::text THEN 305
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '00'::text THEN 307
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '01'::text THEN 309
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '02'::text THEN 310
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '03'::text THEN 311
            WHEN a.korek_rincian::text = '16'::text AND a.korek_sub1::text = '04'::text THEN 308
            WHEN a.korek_rincian::text = '17'::text AND a.korek_sub1::text = '00'::text THEN 312
            WHEN a.korek_rincian::text = '18'::text AND a.korek_sub1::text = '00'::text THEN 313
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '00'::text THEN 315
            WHEN a.korek_rincian::text = '19'::text AND a.korek_sub1::text = '01'::text THEN 314
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN 317
            WHEN a.korek_rincian::text = '20'::text AND a.korek_sub1::text = '00'::text THEN 316
            WHEN a.korek_rincian::text = '21'::text AND a.korek_sub1::text = '00'::text THEN 318
            WHEN a.korek_rincian::text = '22'::text AND a.korek_sub1::text = '00'::text THEN 319
            WHEN a.korek_rincian::text = '23'::text AND a.korek_sub1::text = '00'::text THEN 320
            WHEN a.korek_rincian::text = '24'::text AND a.korek_sub1::text = '00'::text THEN 321
            WHEN a.korek_rincian::text = '25'::text AND a.korek_sub1::text = '00'::text THEN 322
            WHEN a.korek_rincian::text = '26'::text AND a.korek_sub1::text = '00'::text THEN 323
            WHEN a.korek_rincian::text = '27'::text AND a.korek_sub1::text = '00'::text THEN 324
            WHEN a.korek_rincian::text = '28'::text AND a.korek_sub1::text = '00'::text THEN 325
            WHEN a.korek_rincian::text = '29'::text AND a.korek_sub1::text = '00'::text THEN 326
            WHEN a.korek_rincian::text = '30'::text AND a.korek_sub1::text = '00'::text THEN 327
            WHEN a.korek_rincian::text = '31'::text AND a.korek_sub1::text = '00'::text THEN 328
            WHEN a.korek_rincian::text = '32'::text AND a.korek_sub1::text = '00'::text THEN 329
            WHEN a.korek_rincian::text = '33'::text AND a.korek_sub1::text = '00'::text THEN 330
            WHEN a.korek_rincian::text = '34'::text AND a.korek_sub1::text = '00'::text THEN 331
            WHEN a.korek_rincian::text = '35'::text AND a.korek_sub1::text = '00'::text THEN 332
            WHEN a.korek_rincian::text = '36'::text AND a.korek_sub1::text = '00'::text THEN 333
            WHEN a.korek_rincian::text = '37'::text AND a.korek_sub1::text = '00'::text THEN 334
            WHEN a.korek_rincian::text = '38'::text AND a.korek_sub1::text = '00'::text THEN 335
            WHEN a.korek_rincian::text = '39'::text AND a.korek_sub1::text = '00'::text THEN 336
            WHEN a.korek_rincian::text = '40'::text AND a.korek_sub1::text = '00'::text THEN 337
            WHEN a.korek_rincian::text = '41'::text AND a.korek_sub1::text = '00'::text THEN 338
            WHEN a.korek_rincian::text = '42'::text AND a.korek_sub1::text = '00'::text THEN 339
            WHEN a.korek_rincian::text = '43'::text AND a.korek_sub1::text = '00'::text THEN 340
            WHEN a.korek_rincian::text = '44'::text AND a.korek_sub1::text = '00'::text THEN 341
            WHEN a.korek_rincian::text = '45'::text AND a.korek_sub1::text = '00'::text THEN 342
            WHEN a.korek_rincian::text = '46'::text AND a.korek_sub1::text = '00'::text THEN 343
            WHEN a.korek_rincian::text = '47'::text AND a.korek_sub1::text = '00'::text THEN 344
            WHEN a.korek_rincian::text = '48'::text AND a.korek_sub1::text = '00'::text THEN 345
            WHEN a.korek_rincian::text = '49'::text AND a.korek_sub1::text = '00'::text THEN 346
            WHEN a.korek_rincian::text = '50'::text AND a.korek_sub1::text = '00'::text THEN 347
            WHEN a.korek_rincian::text = '51'::text AND a.korek_sub1::text = '00'::text THEN 348
            WHEN a.korek_rincian::text = '52'::text AND a.korek_sub1::text = '00'::text THEN 349
            WHEN a.korek_rincian::text = '53'::text AND a.korek_sub1::text = '00'::text THEN 350
            WHEN a.korek_rincian::text = '54'::text AND a.korek_sub1::text = '00'::text THEN 351
            ELSE 351
        END AS t_id_rekening,
    'Induk'::text AS t_status_objek,
    a.wp_wr_nama AS t_nama_objek,
    a.wp_wr_almt AS t_jalan_tobjek,
    '000'::text AS t_rt_objek,
    '000'::text AS t_rw_objek,
        CASE
            WHEN c.wp_wr_kd_camat::text = '1'::text THEN 0
            ELSE 81
        END AS t_id_provinsi_objek,
        CASE
            WHEN c.wp_wr_kd_camat::text = '1'::text THEN 0
            ELSE 8171
        END AS t_id_kabupaten_objek,
        CASE
            WHEN c.wp_wr_kd_camat::text = '2'::text THEN 8171020
            WHEN c.wp_wr_kd_camat::text = '3'::text THEN 8171030
            WHEN c.wp_wr_kd_camat::text = '4'::text THEN 8171031
            WHEN c.wp_wr_kd_camat::text = '5'::text THEN 8171010
            WHEN c.wp_wr_kd_camat::text = '6'::text THEN 8171021
            ELSE 0
        END AS t_id_kecamatan_objek,
        CASE
            WHEN c.wp_wr_kd_lurah::text = '2'::text THEN 46409
            WHEN c.wp_wr_kd_lurah::text = '3'::text THEN 46400
            WHEN c.wp_wr_kd_lurah::text = '4'::text THEN 46399
            WHEN c.wp_wr_kd_lurah::text = '5'::text THEN 46398
            WHEN c.wp_wr_kd_lurah::text = '6'::text THEN 46408
            WHEN c.wp_wr_kd_lurah::text = '7'::text THEN 46401
            WHEN c.wp_wr_kd_lurah::text = '8'::text THEN 46404
            WHEN c.wp_wr_kd_lurah::text = '9'::text THEN 46403
            WHEN c.wp_wr_kd_lurah::text = '10'::text THEN 46407
            WHEN c.wp_wr_kd_lurah::text = '11'::text THEN 46411
            WHEN c.wp_wr_kd_lurah::text = '12'::text THEN 46410
            WHEN c.wp_wr_kd_lurah::text = '13'::text THEN 46406
            WHEN c.wp_wr_kd_lurah::text = '14'::text THEN 46405
            WHEN c.wp_wr_kd_lurah::text = '15'::text THEN 46402
            WHEN c.wp_wr_kd_lurah::text = '16'::text THEN 46426
            WHEN c.wp_wr_kd_lurah::text = '17'::text THEN 46423
            WHEN c.wp_wr_kd_lurah::text = '18'::text THEN 46424
            WHEN c.wp_wr_kd_lurah::text = '19'::text THEN 46425
            WHEN c.wp_wr_kd_lurah::text = '20'::text THEN 46421
            WHEN c.wp_wr_kd_lurah::text = '21'::text THEN 46422
            WHEN c.wp_wr_kd_lurah::text = '22'::text THEN 46420
            WHEN c.wp_wr_kd_lurah::text = '23'::text THEN 46414
            WHEN c.wp_wr_kd_lurah::text = '24'::text THEN 46417
            WHEN c.wp_wr_kd_lurah::text = '25'::text THEN 46412
            WHEN c.wp_wr_kd_lurah::text = '26'::text THEN 46419
            WHEN c.wp_wr_kd_lurah::text = '27'::text THEN 46418
            WHEN c.wp_wr_kd_lurah::text = '28'::text THEN 46416
            WHEN c.wp_wr_kd_lurah::text = '29'::text THEN 46415
            WHEN c.wp_wr_kd_lurah::text = '30'::text THEN 46413
            WHEN c.wp_wr_kd_lurah::text = '31'::text THEN 46397
            WHEN c.wp_wr_kd_lurah::text = '32'::text THEN 46394
            WHEN c.wp_wr_kd_lurah::text = '33'::text THEN 46396
            WHEN c.wp_wr_kd_lurah::text = '34'::text THEN 46395
            WHEN c.wp_wr_kd_lurah::text = '35'::text THEN 46386
            WHEN c.wp_wr_kd_lurah::text = '36'::text THEN 46389
            WHEN c.wp_wr_kd_lurah::text = '37'::text THEN 46389
            WHEN c.wp_wr_kd_lurah::text = '38'::text THEN 46387
            WHEN c.wp_wr_kd_lurah::text = '39'::text THEN 46393
            WHEN c.wp_wr_kd_lurah::text = '40'::text THEN 46388
            WHEN c.wp_wr_kd_lurah::text = '41'::text THEN 46391
            WHEN c.wp_wr_kd_lurah::text = '42'::text THEN 46392
            WHEN c.wp_wr_kd_lurah::text = '43'::text THEN 46390
            WHEN c.wp_wr_kd_lurah::text = '44'::text THEN 46379
            WHEN c.wp_wr_kd_lurah::text = '45'::text THEN 46384
            WHEN c.wp_wr_kd_lurah::text = '46'::text THEN 46382
            WHEN c.wp_wr_kd_lurah::text = '47'::text THEN 46378
            WHEN c.wp_wr_kd_lurah::text = '48'::text THEN 46380
            WHEN c.wp_wr_kd_lurah::text = '49'::text THEN 46383
            WHEN c.wp_wr_kd_lurah::text = '50'::text THEN 46385
            WHEN c.wp_wr_kd_lurah::text = '51'::text THEN 46381
            ELSE 0
        END AS t_id_kelurahan_objek,
    c.wp_wr_kodepos AS t_kode_pos_objek,
    c.t_latitudeobjek AS t_latitude_objek,
    c.t_longitudeobjek,
    ''::text AS t_nik_pj,
        c.wp_wr_nama_milik AS t_nama_pj,
    c.wp_wr_almt_milik AS t_alamat_pj,
    ''::text AS t_jabatan_pj,
    ''::text AS t_email_pj,
    c.wp_wr_telp_milik AS t_no_hp_pj,
    '1'::text AS created_by,
    c.wp_wr_status_aktif AS t_statusobjek,
    ''::text AS t_foto_objek,
    c.wp_wr_tgl_kartu::timestamp without time zone AS created_at,
    c.wp_wr_tgl_kartu::timestamp without time zone AS updated_at,
    ''::text AS deleted_at,
    't'::text AS t_bulan
   FROM v_spt_one_one a
     LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = a.spt_id
     LEFT JOIN wp_wr c ON c.wp_wr_id = a.spt_idwpwr
  WHERE a.spt_jenis_pajakretribusi = 10
  GROUP BY a.spt_idwpwr, c.wp_wr_tgl_terima_form, a.wp_wr_nama, a.wp_wr_almt, c.wp_wr_kd_camat, c.wp_wr_kd_lurah, a.wp_wr_kabupaten, c.wp_wr_telp, a.spt_jenis_pajakretribusi, c.wp_wr_kodepos, c.t_latitudeobjek, c.t_longitudeobjek, c.wp_wr_pejabat, c.wp_wr_status_aktif, c.wp_wr_telp_milik, c.wp_wr_tgl_kartu, a.korek_rincian, a.korek_sub1,c.wp_wr_nama_milik ,c.wp_wr_almt_milik
  ORDER BY c.wp_wr_tgl_terima_form
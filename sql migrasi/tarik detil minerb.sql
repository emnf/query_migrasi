SELECT
 row_number() OVER (ORDER BY "00_03_transaksi_minerba_detil".id) AS id,
	"00_03_transaksi_minerba_detil".uuid, 
	"00_00_00_00_transaksi_all".idbaru AS t_id_transaksi, 
	"00_03_transaksi_minerba_detil".t_id_rekening, 
	"00_03_transaksi_minerba_detil".t_volume, 
	"00_03_transaksi_minerba_detil".t_hargapasaran, 
	"00_03_transaksi_minerba_detil".t_tarif_persen, 
	"00_03_transaksi_minerba_detil".t_jmlh_pajak as t_jmlhpajak, 
	"00_03_transaksi_minerba_detil".t_jmlh_pembayaran, 
	"00_03_transaksi_minerba_detil".t_jmlh_bunga, 
	"00_03_transaksi_minerba_detil".created_at, 
	"00_03_transaksi_minerba_detil".updated_at
FROM
	"00_03_transaksi_minerba_detil"
	LEFT JOIN
	"00_00_00_00_transaksi_all"
	ON 
		"00_03_transaksi_minerba_detil".t_id_transaksi = "00_00_00_00_transaksi_all".id2
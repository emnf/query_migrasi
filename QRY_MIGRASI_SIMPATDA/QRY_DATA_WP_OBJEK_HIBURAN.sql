SELECT 
'' AS t_idobjek,
A.spt_idwpwr as t_idwp,
'' as t_noobjek,
c.wp_wr_tgl_terima_form as t_tgldaftarobjek,
A.wp_wr_nama as t_namaobjekpj,
A.wp_wr_almt as t_alamatobjekpj,
A.wp_wr_nama as t_namaobjek,
A.wp_wr_almt as t_alamatobjek,
'000' as t_rtobjek,
'000' as t_rwobjek,
A.wp_wr_kd_camat as t_kecamatanobjek,
A.wp_wr_kd_lurah as t_kelurahanobjek,
A.wp_wr_kabupaten as t_kabupatenobjek,
c.wp_wr_telp as t_notelpobjek,
A.spt_jenis_pajakretribusi as t_jenisobjek,
c.wp_wr_kodepos as t_kodeposobjek,
c.wp_wr_lat as t_latitudeobjek,
c.wp_wr_lng as t_longitudeobjek,
'' as t_gambarobjek,
c.wp_wr_pejabat as t_operatorid,
A.korek_id as t_korekobjek,
'' as t_tipeusaha,
c.wp_wr_status_aktif as t_statusobjek,
'' as t_operatoridtutup

FROM
	v_spt_one_one A 
	LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = A.spt_id 
	LEFT JOIN wp_wr c ON c.wp_wr_id = A.spt_idwpwr
WHERE
	A.spt_jenis_pajakretribusi = 3
GROUP BY
	A.spt_idwpwr,
	c.wp_wr_tgl_terima_form,
	A.wp_wr_nama,
	A.wp_wr_almt,
	A.wp_wr_nama,
	A.wp_wr_almt,
	A.wp_wr_kd_camat,
	A.wp_wr_kd_lurah,
	A.wp_wr_kabupaten,
	c.wp_wr_telp,
	A.spt_jenis_pajakretribusi,
	c.wp_wr_kodepos,
	c.wp_wr_lat,
	c.wp_wr_lng,
	c.wp_wr_pejabat,
	A.korek_id,
	c.wp_wr_status_aktif

ORDER BY c.wp_wr_tgl_terima_form
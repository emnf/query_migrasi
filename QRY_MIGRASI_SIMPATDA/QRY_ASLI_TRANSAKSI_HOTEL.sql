SELECT A
	.*,
	b.netapajrek_id,
	b.netapajrek_tgl,
	b.netapajrek_kohir,
	b.netapajrek_tgl_jatuh_tempo,
CASE
		A.spt_jenis_pemungutan 
		WHEN 1 THEN
		( SELECT C.setorpajret_tgl_bayar FROM v_setoran_khusus_self C WHERE C.sprsd_id_spt = A.spt_id ORDER BY C.setorpajret_tgl_bayar ASC LIMIT 1 ) 
		WHEN 2 THEN
		( SELECT C.setorpajret_tgl_bayar FROM v_pembantu_harian C WHERE C.setorpajret_id_penetapan = b.netapajrek_id ORDER BY C.setorpajret_tgl_bayar ASC LIMIT 1 ) 
	END AS setorpajret_tgl_bayar 
FROM
	v_spt_one_one
	A LEFT JOIN penetapan_pajak_retribusi b ON b.netapajrek_id_spt = A.spt_id 
WHERE
	A.spt_jenis_pajakretribusi = 1
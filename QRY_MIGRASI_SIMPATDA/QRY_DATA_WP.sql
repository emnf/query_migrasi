SELECT
	wp_wr_id AS t_idwp, 
	wp_wr_tgl_terima_form AS t_tgldaftar, 
	UPPER(wp_wr_jenis) AS t_jenispendaftaran, 
	wp_wr_gol AS t_bidangusaha, 
	wp_wr_no_urut::INT AS t_nopendaftaran, 
	wp_wr_no_urut::INT AS t_nik, 
	case
		when wp_wr_gol = 2 then wp_wr_nama_milik
		else wp_wr_nama
	end AS t_nama, 
	case 
		when wp_wr_gol = 2 then wp_wr_almt_milik
		else wp_wr_almt 
	end AS t_alamat, 
	'000' AS t_rt, 
	'000' AS t_rw, 
	wp_wr_kd_lurah AS t_kelurahan, 
	wp_wr_kd_camat AS t_kecamatan, 
	case
		when wp_wr_gol = 2 then wp_wr_lurah_milik
		else wp_wr_lurah 
	end AS t_kelurahanluar, 
	case 
		when wp_wr_gol = 2 then wp_wr_camat_milik
		else wp_wr_camat
	end AS t_kecamatanluar, 
	case 
		when wp_wr_gol = 2 then wp_wr_kabupaten_milik
		else wp_wr_kabupaten
	end AS t_kabupaten, 
	wp_wr_telp as t_notelp,
	wp_wr_kodepos as t_kodepos,
	'' as t_email,
	wp_wr_pejabat as t_operatorid,
	'' AS is_deluser,
	case
		when wp_wr_gol = 2 then wp_wr_nama
		else NULL
	end as t_nama_badan,
	case
		when wp_wr_gol = 2 then wp_wr_almt
		else NULL
	end as t_jalan_badan,
	'000' as t_rt_badan,
	'000' as t_rw_badan,
	case
		when wp_wr_gol = 2 then wp_wr_kd_camat
		else '0'
	end as t_kecamatan_badan,
	case
		when wp_wr_gol = 2 then wp_wr_kd_lurah
		else '0'
	end as t_kelurahan_badan,
	case
		when wp_wr_gol = 2 then wp_wr_kabupaten
		else wp_wr_kabupaten
	end as t_kabupaten_badan,
	case
		when wp_wr_gol = 2 then wp_wr_camat
		else NULL
	end as t_kecamatan_badan_luar,
	case
		when wp_wr_gol = 2 then wp_wr_lurah
		else NULL
	end as t_kelurahan_badan_luar,
	wp_wr_status_aktif AS t_status_wp, 
	wp_wr_tgl_tutup AS t_tgl_tutup, 
	'' as t_ket_tutup,
	'' as t_operatorid_tutup,
	'' as t_noberita,
	wp_wr_tgl_buka AS t_tgl_buka,
	'' as t_ket_buka,
	'' as t_operatorid_buka,
	'' as t_noberita_buka,
	'' as t_photowp,
	'' as t_tglcetak_sk,
	'' as t_nomor_sk,
	'' as t_operatorcetak_sk
FROM
	wp_wr
ORDER BY
	wp_wr_tgl_terima_form ASC, 
	wp_wr_id ASC